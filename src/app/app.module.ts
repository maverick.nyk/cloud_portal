import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes } from '@angular/router';
import { HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { UserModule } from './modules/user_management/user.module';
import { UserComponent} from './modules/user_management/user.component';


@NgModule({
  declarations: [
    AppComponent,
   ],
  imports: [
    BrowserModule,
    HttpModule,
    UserModule,
    RouterModule.forRoot([
      {
         path: 'users',
         component: UserComponent,
      }
     
   ])
],

  bootstrap: [AppComponent]
})
export class AppModule { }
