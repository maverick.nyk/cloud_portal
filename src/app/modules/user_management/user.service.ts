import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions,Response } from '@angular/http';


@Injectable()
export class UserService {

    constructor(public http: Http) {
      }

      sendCreateRequest(url:string,data:any){
        var headers = new Headers();
         headers.append('content-type', 'application/json');
         this.http.post(url, data).subscribe(res => console.log(res.json()));
}
        
      }




export interface PageStatus {
  loading: boolean;
  error: boolean;
  success: boolean;
}