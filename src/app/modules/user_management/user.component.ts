import { Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid";
import {UserService} from "./user.service";
import { Http } from '@angular/http';

@Component({
    selector: 'manage-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
  })
  export class UserComponent implements OnInit {
   
 //  will be used to  
 //   1.assign column definitions to child component
 //   2.override any component specific gridoptions 
 private gridOptions: GridOptions={};
 // will be used to override default height of grid
 private gridHeight : Number;
 // will be used to override default width of grid
 private gridWidth : Number;
 // will be used to provide grid data url
 private gridDataUrl : String="http://172.16.155.31:8080/portal/api/user?page=0&size=1000&sortParam=id&sortDirection=asc&searchKey=";

 // will be used to override default set of params with copmonent specific query param in Portal app.
 private gridDataUrlQueryParams : {
     headerName :  String,
     value : String
 }[];

 constructor(private service:UserService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [
        {
            width:20,
            headerCheckboxSelection: true,
            headerCheckboxSelectionFilteredOnly: true,
            checkboxSelection: true
        },

        {
            headerName :  'First Name',
            field : 'firstName',
            
            
        },
        {
            headerName :  'Last Name',
            field : 'lastName',
            
        },
        {
            headerName :  'Email',
            field : 'email'
            
            
        },
        {
            headerName :  'Role',
            field : 'role.name'
            
        },
        {
            headerName :  'Customer',
            field : 'company.name'
            
        },
        {
            headerName :  'Status',
            field : 'status'
            
        }
        ];

    


 }

 createUser(userCount:Number){
     var data;
    for(var i=1;i<=userCount;i++){

   data = {
    "company": {
                  "id": "5a53430722deb71e04a1187a"
    },
    "email": "maverick_nyk"+i+"@gmail.com",
    "firstName": "Maverick"+i,
    "frontRoom": {
                  "id":"5a54696422deb726487c5f5b"
    },
    "handset": "1",
    "handsetRecMix": "Mixed",
    "hardware": "IQ/MAX Touch Primary Module",
    "homeDataCenter": {
     "id": "5a5342e722deb71e04a11879"
    },

    "lastName": "Nyk"+i,
  
    "networkEntitlement": "Inet",
    "networkPreference": "Inet",
  
    "productName": "IQMAX",
    "products": [
                  {
                    "id": "1",
                    "name": "IQMAX"
                  }
    ],
    "profileType": "Trade",
    "recording": "Yes",
    "role": {
                  "id": "3"
    },
  
    "speakerChannels": "32",
    "speakerRec": "1:1",
    "style": "G-Style Rocker"

  }



        this.service.sendCreateRequest("http://localhost:8080/portal/api/user",data);
    }

 }


    ngOnInit(){

    }

  }