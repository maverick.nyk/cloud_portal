import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { HttpModule} from '@angular/http';
import {AgGridModule} from 'ag-grid-angular/main';

import {UserComponent} from './user.component';
import { GridComponent } from '../../components/grid/grid.component';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { GridService } from '../../components/grid/grid.service';
import {UserService} from './user.service';

@NgModule({
  declarations: [
   GridComponent,
   HeaderComponent,
   FooterComponent,
   UserComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AgGridModule.withComponents(
      [GridComponent]
  )],
  providers:[UserService,GridService]
})
export class UserModule { }
