import { Component, OnInit, Input } from '@angular/core';
import {GridOptions} from "ag-grid";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { forEach } from '@angular/router/src/utils/collection';
import {GridService,PageStatus} from './grid.service'


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
    //Default options
    private gridOptions: GridOptions=<GridOptions>{
                                        animateRows: true,
                                        enableSorting: true,
                                        suppressDragLeaveHidesColumns :true,
                                        groupDefaultExpanded: 1,
                                        enableFilter:true,
                                        pagination: true,
                                        paginationAutoPageSize: true,
                                        colWidth:130,
                                        onGridReady(params) {
                                            this.gridApi = params.api;
                                            this.columnApi = params.columnApi;
                                            this.gridApi.sizeColumnsToFit();
                                            window.onresize = () => {
                                                this.gridApi.sizeColumnsToFit();
                                                }
                                            },
                                        };
    //Grip options input with column definitions and updated options
    @Input() private gridOptionsInput:GridOptions; 
    @Input() private width :Number;
    @Input() private height: Number=380;                                
    @Input() private dataUrl:string;
    @Input() private queryParams : {name:string,value:string}[]=[{name:'page',value:'0'},
         {name:'size',value:'10'},
         {name:'sortParam',value:'id'},
         {name:'sortDirection',value:'asc'},
         {name:'searchKey',value:''}]
    private pageStatus : PageStatus={
        loading : false,
        error : false,
        success:false
    };
    private gridApi;
    
  constructor(private http: Http,private service:GridService) {
    

}

assignGridOptions(gridOptionsInput){
    for (var k in gridOptionsInput){
        if (gridOptionsInput.hasOwnProperty(k)) {
            this.gridOptions[k] = gridOptionsInput[k];
        }
    }
    console.log("GRID OPTIONS",this.gridOptions);
    
}
onQuickFilterChanged(value : string) {
  
    this.gridOptions.api.setQuickFilter(value);
  }
ngOnInit() {
    this.gridOptions.columnDefs = this.gridOptionsInput.columnDefs;
    console.log(this.dataUrl)
    //this.assignGridOptions(this.gridOptionsInput);

    this.pageStatus={
        loading : true,
        error : false,
        success:false
    };
    this.service.getData(this.dataUrl).subscribe(
        (response:any) => {

            this.gridOptions.rowData = response.json().content;  
            //for(var i=0;i<=1200;i++){
            //    this.gridOptions.rowData = this.gridOptions.rowData.concat(response.json().content);  
            //}
            
            this.pageStatus = {
                loading : false,
                error : false,
                success:true
            }
            
         }
     );

}



}
