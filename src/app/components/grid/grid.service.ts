import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class GridService {

    constructor(public http: Http) {
      }

      getData(url:string){
        let headers = new Headers();
        headers.append('content-type', 'application/json');
        return this.http.get(url);
      }
      
}
export interface PageStatus {
  loading: boolean;
  error: boolean;
  success: boolean;
}